# Literature Search Engine

### Steps to run the demo
#### Locally
1. Ensure the path to the documents, and port numbers (if running locally) are set correctly in `index.html`,`IndexBuilder.py` and `Search.py`
2. If the index has not been built yet, run `python3 IndexBuilder.py`
3. Run `python3 SearchEngine.py` to start the server
4. Open `index.html` in the browser if running locally

#### Through server
1. Go to http://chagari2.web.illinois.edu/cs510/
2. The code to run the demo has been hosted on CPanel

Happy searching!

