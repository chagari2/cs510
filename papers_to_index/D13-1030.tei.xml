<abstract>Interpreting anaphoric shell nouns (ASNs) such as this issue and this fact is essential to understanding virtually any substantial natural language text. One obstacle in developing methods for automatically interpreting ASNs is the lack of annotated data. We tackle this challenge by exploiting cataphoric shell nouns (CSNs) whose construction makes them particularly easy to interpret (e.g., the fact that X). We propose an approach that uses automatically extracted antecedents of CSNs as training data to interpret ASNs. We achieve precisions in the range of 0.35 (baseline = 0.21) to 0.72 (baseline = 0.44), depending upon the shell noun.</abstract>
<title>Interpreting Anaphoric Shell Nouns using Antecedents of Cataphoric Shell Nouns as Training Data</title>
<introduction>Anaphors such as this fact and this issue encapsulate complex abstract entities such as propositions, facts, and events. An example is shown below.(1) Here is another bit of advice: Environmental Defense, a national advocacy group, notes that "Mowing the lawn with a gas mower produces as much pollution in half an hour as driving a car 172 miles." This fact may help to explain the recent surge in the sales of the good oldfashioned push mowers or the battery-powered mowers.Here, the anaphor this fact is interpreted with the help of the clausal antecedent marked in bold. The antecedent here is complex because it involves a number of entities and events (e.g., mowing the lawn, a gas mower) and relationships between them, and is abstract because the antecedent itself is not a purely physical entity. The distinguishing property of these anaphors is that they contain semantically rich abstract nouns (e.g., fact in (1)) which characterize and label their corresponding antecedents. Linguists and philosophers have studied such abstract nouns for decades (Vendler, 1968;Halliday and Hasan, 1976;Francis, 1986;Ivanic, 1991;Asher, 1993). Our work is inspired by one such study, namely that of Schmid (2000). Following Schmid, we refer to these abstract nouns as shell nouns, as they serve as conceptual shells for complex chunks of information. Accordingly, we refer to the anaphoric occurrences of shell nouns (e.g., this fact in (1)) as anaphoric shell nouns (ASNs).An important reason for studying ASNs is their ubiquity in all kinds of text. Schmid (2000) observed that shell nouns such as fact, idea, point, and problem were among the 100 most frequently occurring nouns in a corpus of 225 million words of British English. Moreover, ASNs can play several roles in organizing a discourse such as encapsulation of complex information, cohesion, and topic boundary marking. So correct interpretation of ASNs can be an important step for correct interpretation of a discourse, and in a number of NLP applications such as text summarization, information extraction, and non-factoid question answering.Despite their importance, ASNs have not received much attention in Computational Linguistics, and research in this field remains in its earliest stages. At present, the major obstacle is that there is very little annotated data available that could be used to train a supervised machine learning system for robustly interpreting these anaphors, and manual annotation is an expensive and time-consuming task.We tackle this challenge by exploiting a category of examples, as shown in (2), whose construction is particularly easy to interpret.(2) Congress has focused almost solely on the fact that special education is expensive -and that it takes away money from regular education.Here, in contrast with (1), the fact is not anaphoric in the traditional sense, but is an easy case of a forward-looking anaphor -a cataphor. While the resolution process of this fact in (1) is quite challenging as it requires the use of semantics and world knowledge, it is fairly easy to interpret the fact in (2) based on the syntactic structure alone. We refer to these easy-to-interpret cataphoric occurrences of shell nouns as cataphoric shell nouns (CSNs). The interpretation of both ASNs and CSNs will be referred to as antecedent. 1 The antecedent of the fact in (2) is given in the post-nominal that clause. We use the term shell concept to refer to the general notion of a shell noun, i.e., the semantic type of the antecedent. For example, the notion of an issue is an important problem which requires a solution. In this work, we propose an approach to interpret ASNs that exploits unlabelled but easy-to-interpret CSN examples to extract characteristic features associated with the antecedent of different shell concepts. We evaluate our approach using crowdsourcing. Our results show that these unlabelled CSN examples provide useful linguistic properties that help in interpreting ASNs.</introduction>
